# hackass

[![pipeline status](https://gitlab.com/me-learnz/nand2tetris/hackass/badges/master/pipeline.svg)](https://gitlab.com/me-learnz/nand2tetris/hackass/pipelines)

Translator of Hack VM language into a Hack assembly language.
Developed for a course [From NAND to Tetris: Building a Modern Computer From First Principles](http://www.nand2tetris.org).