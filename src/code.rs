//! This module contains tables translating mnemonics (fields of
//! a C-instruction) into their binary form.
//!
//! Tables are stored as [`HashMap`]s .
//!
//! [`HashMap`]: https://doc.rust-lang.org/stable/std/collections/struct.HashMap.html

use std::collections::HashMap;

lazy_static! {
    /// Translations of the `comp` field (28 possibilities).
    pub static ref COMP_BITS: HashMap<&'static str, &'static str> = [
        ("0"  , "0101010"),
        ("1"  , "0111111"),
        ("-1" , "0111010"),
        ("D"  , "0001100"),
        ("!D" , "0001101"),
        ("-D" , "0001111"),
        ("D+1", "0011111"),
        ("D-1", "0001110"),
        ("A"  , "0110000"),
        ("!A" , "0110001"),
        ("-A" , "0110011"),
        ("A+1", "0110111"),
        ("A-1", "0110010"),
        ("M"  , "1110000"),
        ("!M" , "1110001"),
        ("-M" , "1110011"),
        ("M+1", "1110111"),
        ("M-1", "1110010"),
        ("D+A", "0000010"),
        ("D-A", "0010011"),
        ("A-D", "0000111"),
        ("D&A", "0000000"),
        ("D|A", "0010101"),
        ("D+M", "1000010"),
        ("D-M", "1010011"),
        ("M-D", "1000111"),
        ("D&M", "1000000"),
        ("D|M", "1010101"),
    ].iter().cloned().collect();

    /// Translations of the `dest` field (7 possibilities). Default entry for
    /// an empty field (`000`) is not included.
    pub static ref DEST_BITS: HashMap<&'static str, &'static str> = [
        ("M"  , "001"),
        ("D"  , "010"),
        ("MD" , "011"),
        ("A"  , "100"),
        ("AM" , "101"),
        ("AD" , "110"),
        ("AMD", "111"),
    ].iter().cloned().collect();

    /// Translations of the `jump` field (7 possibilities). Default entry for
    /// an empty field (`000`) is not included.
    pub static ref JUMP_BITS: HashMap<&'static str, &'static str> = [
        ("JGT", "001"),
        ("JEQ", "010"),
        ("JGE", "011"),
        ("JLT", "100"),
        ("JNE", "101"),
        ("JLE", "110"),
        ("JMP", "111"),
    ].iter().cloned().collect();
}


#[cfg(test)]
mod tests {
    use super::{COMP_BITS, DEST_BITS, JUMP_BITS};

    #[test]
    fn comp_num_entries() {
        assert_eq!(COMP_BITS.len(), 28,
                   "COMP_BITS has invalid number of entries");
    }

    #[test]
    fn dest_num_entries() {
        assert_eq!(DEST_BITS.len(), 7,
                   "DEST_BITS has invalid number of entries");
    }

    #[test]
    fn jump_num_entries() {
        assert_eq!(JUMP_BITS.len(), 7,
                   "JUMP_BITS has invalid number of entries");
    }
}