#[macro_use]
extern crate lazy_static;
extern crate clap;
#[cfg(test)]
extern crate tempdir;

use std::path::{Path, PathBuf};
use std::fs::{File, remove_file};
use std::io::{Write, BufWriter};

use clap::{Arg, App};

use parser::{Parser, CmdType};
use code::{DEST_BITS, COMP_BITS, JUMP_BITS};
use symbol_table::SymbolTable;

pub mod parser;
pub mod code;
pub mod symbol_table;

// Check validity of a symbol
fn check_valid(sym: &str) -> Result<String, String> {
    let char_is_valid = |c: char| {
        let allowed_chars = vec![':', '$', '_', '.'];
        c.is_alphabetic() || c.is_numeric() || allowed_chars.contains(&c)
    };

    let mut err = String::new();
    if sym.is_empty() {
        err = String::from("Symbol is empty.");
    }

    let mut sym_iter = sym.chars().enumerate();
    while err.is_empty() {
        match sym_iter.next() {
            Some((i, c)) => {
                if i==0 && c.is_numeric() {
                    err = format!("Symbol `{}` starts with a digit.",
                                  sym);
                } else if !char_is_valid(c) {
                    err = format!("Char `{}` in symbol `{}` is invalid.",
                                  c, sym);
                }
            },
            None => break,
        }
    }

    match err.is_empty() {
        true => Ok(sym.into()),
        false => Err(err)
    }
}

fn main() {
    // CLI
    let matches = App::new("hackass")
                          .version("0.3.0")
                          .author("Jakub Žádník <kubouch@gmail.com>")
                          .about("Assembler for a Hack assembly language \
                                  from www.nand2tetris.org.")
                          .arg(Arg::with_name("INPUT")
                               .help("Input assembly file.")
                               .required(true)
                               .index(1))
                          .arg(Arg::with_name("output")
                               .short("o")
                               .long("output")
                               .value_name("FILE")
                               .help("Optional output file.")
                               .takes_value(true))
                          .arg(Arg::with_name("verbose")
                               .short("v")
                               .long("verbose")
                               .help("Print info, otherwise be silent. \
                                      Errors are still shown."))
                          .get_matches();

    let verbose = matches.is_present("verbose");

    let inp_file = Path::new(matches.value_of("INPUT").unwrap());
    if verbose {
        println!("Input assembly file: {}", inp_file.display());
    }

    let out_file = match matches.value_of("output") {
        None => inp_file.with_extension("hack"),
        Some(o_arg) => PathBuf::from(o_arg),
    };
    if verbose {
        println!("Output saved to: {}", out_file.display());
    }

    // Open input file
    let fh_in = File::open(&inp_file);
    let fh_in = match fh_in {
        Ok(fh) => fh,
        Err(e) => panic!("Error opening {}:\n{}\n", inp_file.display(), e)
    };
    // Create a parser
    let mut p = Parser::new(fh_in);  // takes ownership of fh_in

    // First pass: translate symbols into addresses and save to SymbolTable
    let mut symbol_table = SymbolTable::new();
    let mut next_inst = 0;
    while p.has_more_commands() {
        p.advance();

        match p.cmd {
            Some(CmdType::A(_)) => {
                next_inst += 1;
            },
            Some(CmdType::L(ref symbol)) => {
                symbol_table.add_entry(&check_valid(symbol).unwrap(),
                                       next_inst);
            },
            Some(CmdType::C(_)) => {
                next_inst += 1;
            }
            None => (),
        }
    }

    // Reset Parser
    p.reset();

    // Open output file
    let fh_out = File::create(&out_file);
    let fh_out = match fh_out {
        Ok(fh) => fh,
        Err(e) => panic!("Error creating {}:\n{}\n", out_file.display(), e)
    };
    // Create a writer
    let mut writer = BufWriter::new(fh_out);

    // Second pass: translate into machine language
    while p.has_more_commands() {
        p.advance();

        match p.cmd {

            Some(CmdType::A(ref symbol)) => {
                let addr = match symbol.parse::<u16>() {
                    Ok(addr) => addr,
                    Err(_) => {
                        let valid_sym = check_valid(symbol).unwrap();
                        if symbol_table.contains(&valid_sym) {
                            *symbol_table.get_addr(&valid_sym).unwrap()
                        } else {
                            symbol_table.add_variable(&valid_sym)
                        }
                    }
                };

                let addr = format!("{:016b}\n", addr);
                writer.write_all(addr.as_bytes())
                    .expect("Unable to write to output file");
            },

            Some(CmdType::L(_)) => (),

            Some(CmdType::C(ref c_cmd)) => {
                let dest_bin = match c_cmd.dest {
                    Some(ref field) => DEST_BITS.get(field.as_str())
                        .expect(&format!("`{}` is not a valid `dest` field.",
                                &field)),
                    None => "000",
                };
                let jump_bin = match c_cmd.jump {
                    Some(ref field) => JUMP_BITS.get(field.as_str())
                        .expect(&format!("`{}` is not a valid `jump` field.",
                                &field)),
                    None => "000",
                };
                let comp_bin = COMP_BITS.get(c_cmd.comp.as_str())
                    .expect(&format!("`{}` is not a valid `comp` field.",
                            &c_cmd.comp));
                let mut cmd_bin = String::from("111");
                cmd_bin.push_str(comp_bin);
                cmd_bin.push_str(dest_bin);
                cmd_bin.push_str(jump_bin);
                cmd_bin.push('\n');
                writer.write_all(cmd_bin.as_bytes())
                    .expect("Unable to write to output file");
            }
            None => (),
        };
    }

    if let None = p.cmd {
        remove_file(&out_file)
            .expect(&format!("Error removing {}", out_file.display()));
        panic!(format!("File `{}` does not contain any instructions. No \
                        output was written", inp_file.display()));
    }

    if verbose {
        println!{"Wrote {} instructions.", next_inst};
    }
}


#[cfg(test)]
mod tests {
    use super::check_valid;

    #[test]
    fn check_symbol_valid() {
        let inp = vec!["ax1AX", "unicode:öÄýŘž", "special:.$_"];
        for s in inp {
            assert_eq!(check_valid(s), Ok(String::from(s)));
        }
    }

    #[test]
    #[should_panic(expected = "Symbol is empty.")]
    fn check_symbol_err_empty() {
        let inp = "";
        check_valid(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Symbol `0xx` starts with a digit.")]
    fn check_symbol_err_digit() {
        let inp = "0xx";
        check_valid(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Char `-` in symbol `-125` is invalid.")]
    fn check_symbol_err_invalid_char_1() {
        let inp = "-125";
        check_valid(inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Char `+` in symbol `abc+x` is invalid.")]
    fn check_symbol_err_invalid_char_2() {
        let inp = "abc+x";
        check_valid(inp).unwrap();
    }
}