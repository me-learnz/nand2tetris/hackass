//! `Parser` reads an opened input file, detects instruction types and matches
//! them to their fields.
//!
//! The data type used for storing the instruction information is [`CmdType`].
//!
//! `Parser` checks whether the instruction fits the assembly format. However,
//! it does not check the validity of the fields and symbols. They can still
//! contain errors (such as not allowed characters) which need to be resolved
//! outside of `Parser`.
//!
//! # Examples
//!
//! Create a new parser and print all decoded commands:
//!
//! ```rust, no_run
//! # fn foo() -> std::io::Result<()> {
//! // Write some assembly file
//! let mut f = File::create("foo.asm")?;
//! f.write_all(b"// Some comment\n\
//!               \n\
//!               @R0\n\
//!               D=M    // Read from R0\n\
//!               @2\n\
//!               M=D+A\n\
//!               (LOOP) // Endless loop\n\
//!               @LOOP\n\
//!               0;JMP\n")?;
//!
//! // Open the file and create a `Parser`.
//! let fh = File::open("foo.asm")?;
//! let mut p = Parser::new(fh);
//!
//! while p.has_more_commands() {
//!     p.advance();
//!
//!     println!("{:?}", p.cmd);
//! }
//!
//! // Should print:
//! // Some(A("R0"))
//! // Some(C(CommandC { dest: Some("D"), comp: "M", jump: None }))
//! // Some(A("2"))
//! // Some(C(CommandC { dest: Some("M"), comp: "D+A", jump: None }))
//! // Some(L("LOOP"))
//! // Some(A("LOOP"))
//! // Some(C(CommandC { dest: None, comp: "0", jump: Some("JMP") }))
//! # Ok(())
//! # }
//! ```

use std::io;
use std::io::{BufRead, BufReader, Seek, SeekFrom};
use std::fmt;
use std::fs::File;

/// The `Parser` struct used for parsing input Hack assembly file.
///
/// The `Parser` operates on an opened file. It reads the file line by line,
/// detecting whether the line is a command, which type of commands and what
/// fields have been used by the command.
///
/// The validity of decoded fields is not tested. Also `Parser` does not
/// provide a translation into a machine language. That is handled by the
/// [`Code`] module.
///
///
/// [`Code`]: ../code/index.html
pub struct Parser {
    /// Current parsed command is stored in `cmd`.
    ///
    /// The value is initialized to [`None`] when `Parser` is created.
    ///
    /// [`None`]: https://doc.rust-lang.org/stable/std/option/enum.Option.html#variant.None
    pub cmd: Option<CmdType>,
    next_cmd: Option<CmdType>,
    reader: BufReader<File>,

}

impl Parser {
    /// Create a new instance of `Parser`.
    ///
    /// Needs a file handle of an opened file. Then, it creates a new
    /// [`BufReader`] which is used for reading the file.
    ///
    /// The method also performs a search for the next command.
    ///
    /// [`BufReader`]: https://doc.rust-lang.org/stable/std/io/struct.BufReader.html
    pub fn new(fh: File) -> Parser {
        let mut parser = Parser { cmd: None,
                                  next_cmd: None,
                                  reader: io::BufReader::new(fh),
                                };
        parser.next_command();
        parser
    }

    /// Checks whether there are more commands in the assembly file.
    ///
    /// This function returns `true` if there is at least one command in the
    /// assembly file which has not been read.
    pub fn has_more_commands(&self) -> bool {
        self.next_cmd.is_some()
    }

    /// Save the next available command in the [`cmd`] field of `Parser`.
    ///
    /// First, the method finds a next command, skipping empty lines or lines
    /// with only comments. Then, a value of a private variable (the one read
    /// by [`has_more_commands`]) is updated, depending on whether a command
    /// was found or not. If a command was found, it gets stored in `Parser's`
    /// [`cmd'] field. Otherwise, nothing else happens.
    ///
    /// # Panics
    ///
    /// Panics when
    ///
    /// * both `dest` and `jump` are missing from a C-command.
    /// * C-command does not have a `comp` field (it is empty).
    ///
    /// [`cmd`]: #structfield.cmd
    /// [`has_more_commands`]: #method.has_more_commands
    pub fn advance(&mut self) {
        if self.has_more_commands() {
            self.cmd = self.next_cmd.take();
        }
        self.next_command();
    }

    /// Reset `Parser` into the same state as if it was just created.
    pub fn reset(&mut self) {
        self.cmd = None;
        self.next_cmd = None;
        self.reader.seek(SeekFrom::Start(0)).unwrap();
        self.next_command();
    }

    fn next_line_trimmed(&mut self) -> LineType {
        let mut line = String::new();
        self.reader.read_line(&mut line).expect("Error reading line.");

        if line.is_empty() {
            LineType::EOF
        } else {
            line = trim_line(&line);
            if line.is_empty() {
                LineType::Empty
            } else {
                LineType::Line(line)
            }
        }
    }

    fn next_command(&mut self) {
        while self.next_cmd.is_none() {
            match self.next_line_trimmed() {
                LineType::EOF => {
                    self.next_cmd = None;
                    break;
                },
                LineType::Empty => (),
                LineType::Line(cmd) => {
                    let cmd = decode_command(&cmd).unwrap();
                    self.next_cmd = Some(cmd);
                    break;
                },
            }
        }
    }
}

// Enums

/// `CmdType` is a custom type for storing a decoded command.
///
/// It captures information about the command's type (A, L or C).
/// In case of an A- or L- commands, the corresponding symbol is stored.
/// In case of a C-command, it stores its contents into a mandatory `comp`
/// field and optional `dest` and `jump` fields.
#[derive(Debug)]
pub enum CmdType {
    A(String),
    C(CommandC),
    L(String),
}

/// Struct containing expected fields of a C-command: `dest`, `comp`, `jump`.
///
/// `dest` and `jump` fields are optional but at least one of them has to be
/// [`Some`]. `comp` field is mandatory.
///
/// [`Some`]: https://doc.rust-lang.org/stable/std/option/enum.Option.html#variant.Some
#[derive(Debug)]
pub struct CommandC {
    pub dest: Option<String>,
    pub comp: String,
    pub jump: Option<String>,
}

impl fmt::Display for CmdType {
    /// Display command as:
    /// `@symbol` for A-command,
    /// `(symbol)` for L-command or
    /// `dest=comp;jump` for C-command (this one is crude).
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CmdType::A(ref symbol) => write!(f, "@{}", symbol),
            CmdType::L(ref symbol) => write!(f, "({})", symbol),
            CmdType::C(ref c_cmd) => {
                write!(f, "{}={};{}",
                       c_cmd.dest.clone().unwrap_or(String::from("")),
                       c_cmd.comp,
                       c_cmd.jump.clone().unwrap_or(String::from("")))
            }
        }
    }
}

// Used in command decoding
enum LineType {
    EOF,
    Empty,
    Line(String),
}

// Helper functions

/// Remove comments and all surrounding whitespace from a line.
fn trim_line(line: &str) -> String {
    match line.find("//") {
        Some(pos) => line[..pos].trim().into(),
        None => line.trim().into(),
    }
}

/// Decode a command into A-, C- or L- type with corresponding fields.
fn decode_command(s: &str) -> Result<CmdType, String> {
    if s.starts_with('@') {
        let symbol = String::from(&s[1..]);
        Ok(CmdType::A(symbol))
    } else if s.starts_with('(') && s.ends_with(')') {
        let symbol = String::from(&s[1..s.len()-1]);
        Ok(CmdType::L(symbol))
    } else {
        let mut dest = None;
        let mut jump = None;
        let mut comp = String::new();

        match (s.find('='), s.find(';')) {
            (Some(dpos), Some(jpos)) => {
                dest = Some(String::from(&s[..dpos]));
                jump = Some(String::from(&s[jpos+1..]));
                comp.push_str(&s[dpos+1..jpos]);
            },
            (Some(dpos), None) => {
                dest = Some(String::from(&s[..dpos]));
                comp.push_str(&s[dpos+1..]);
            },
            (None, Some(jpos)) => {
                jump = Some(String::from(&s[jpos+1..]));
                comp.push_str(&s[..jpos]);
            },
            (None, None) => (),
        };

        if dest.is_none() && jump.is_none() {
            Err(format!("Both `dest` and `jump` are missing from command \
                        `{}`.", &s))
        } else if comp.is_empty() {
            Err(format!("Command `{}` does not have a `comp` field.", &s))
        } else {
            Ok(CmdType::C(CommandC{ dest, comp: String::from(comp), jump }))
        }
    }
}


#[cfg(test)]
mod tests {
    use std::io::Write;
    use std::fs::File;
    use tempdir::TempDir;

    use super::{CmdType, Parser, decode_command, trim_line};

    #[test]
    // Testing parser's public API
    fn parser_read() {
        let dir = TempDir::new("hackass_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut f = File::create(&file_path).unwrap();
        f.write_all(b"// This is a testing assembly file\n\
                      // Some empty lines\n\
                      \n\
                      \t   \n\
                      \t@R0\n\
                      \tD=M  // Read from R0\n\
                      \t@2\n\
                      \tD=D+A\n\
                      \t@var\n\
                      \tM=D\n\
                      (LOOP)\n\
                      \t@LOOP\n\
                      \t0;JMP").unwrap();
        f.sync_all().unwrap();

        let fh = File::open(&file_path).unwrap();
        let mut p = Parser::new(fh);
        // No command is read
        assert!(p.cmd.is_none(),
                format!("01: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "01: No more commands found.");
        // Check next command again
        assert!(p.has_more_commands(), "02: No more commands found.");
        // Read 1st instruction
        p.advance();
        assert!(p.cmd.is_some(),
                format!("03: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "03: No more commands found.");
        // Read 2nd, 3rd, etc. instructions till the end
        p.advance();
        assert!(p.cmd.is_some(),
                format!("04: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "04: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("05: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "05: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("06: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "06: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("07: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "07: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("08: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "08: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("09: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "09: No more commands found.");
        p.advance();
        assert!(p.cmd.is_some(),
                format!("10: `p.cmd` is {:?}.", p.cmd));
        assert!(p.has_more_commands(), "10: No more commands found.");
        // Read the last instruction
        p.advance();
        match p.cmd {
            Some(CmdType::C(ref c_cmd)) => {
                assert_eq!(c_cmd.dest, None,
                           "11: Wrong `dest` field.");
                assert_eq!(c_cmd.comp, String::from("0"),
                           "11: Wrong `comp` field.");
                assert_eq!(c_cmd.jump, Some(String::from("JMP")),
                           "11: Wrong `jump` field.");
            },
            _ => panic!("11: `p.cmd` is {:?}", p.cmd),
        }
        assert!(!p.has_more_commands(), "11: More commands were found.");
        // Next is EOF
        p.advance();
        assert!(p.cmd.is_some(),
                format!("12: `p.cmd` is {:?}.", p.cmd));
        assert!(!p.has_more_commands(), "12: More commands were found.");

        dir.close().unwrap();
    }

    #[test]
    fn reset() {
        let dir = TempDir::new("hackass_test").unwrap();
        let file_path = dir.path().join("test.asm");
        let mut f = File::create(&file_path).unwrap();
        f.write_all(b"@R0\n\
                      D=M\n").unwrap();
        f.sync_all().unwrap();
        let fh = File::open(&file_path).unwrap();
        let mut p = Parser::new(fh);

        p.advance();
        p.advance();
        p.reset();
        p.advance();
        match p.cmd {
            Some(CmdType::A(ref symbol)) => {
                assert_eq!(symbol, &String::from("R0"),
                           "Wrong `symbol` field.");
            },
            _ => panic!("`p.cmd` was {:?} after reset and \
                         one advance.", p.cmd),
        }
    }

    #[test]
    fn trimming() {
        let trimmed = trim_line("// Comment line\n");
        assert_eq!(trimmed, "");

        let trimmed = trim_line("Line with // a comment\n");
        assert_eq!(trimmed, "Line with");

        let trimmed = trim_line("\t  Line with whitespace  \n");
        assert_eq!(trimmed, "Line with whitespace");

        let trimmed = trim_line("\n");
        assert_eq!(trimmed, "");

        let trimmed = trim_line("  Multi-line // Complex\nstuff! \t //rules");
        assert_eq!(trimmed, "Multi-line");
    }

    #[test]
    fn decode_valid() {
        // A-instruction
        let inp = "@bagr";
        let out = decode_command(&inp).unwrap();
        if let CmdType::A(symbol) = out {
            assert_eq!(symbol, String::from("bagr"));
        }
        // L-instruciton
        let inp = "(LOOP)";
        let out = decode_command(&inp).unwrap();
        if let CmdType::L(symbol) = out {
            assert_eq!(symbol, String::from("LOOP"));
        }
        // C-instruction (dest + comp)
        let inp = "M=D+A";
        let out = decode_command(&inp).unwrap();
        if let CmdType::C(c_cmd) = out {
            assert_eq!(c_cmd.dest, Some(String::from("M")));
            assert_eq!(c_cmd.comp, String::from("D+A"));
            assert_eq!(c_cmd.jump, None);
        }
        // C-instruction (comp + jump)
        let inp = "D+A;JEQ";
        let out = decode_command(&inp).unwrap();
        if let CmdType::C(c_cmd) = out {
            assert_eq!(c_cmd.dest, None);
            assert_eq!(c_cmd.comp, String::from("D+A"));
            assert_eq!(c_cmd.jump, Some(String::from("JEQ")));
        }
        // C-instruction (dest + comp + jump)
        let inp = "M=D+A;JEQ";
        let out = decode_command(&inp).unwrap();
        if let CmdType::C(c_cmd) = out {
            assert_eq!(c_cmd.dest, Some(String::from("M")));
            assert_eq!(c_cmd.comp, String::from("D+A"));
            assert_eq!(c_cmd.jump, Some(String::from("JEQ")));
        }
    }

    #[test]
    #[should_panic(expected = "Both `dest` and `jump` are missing from \
                               command `D`.")]
    fn decode_err_missing_dest_jump() {
        let inp = "D";
        decode_command(&inp).unwrap();
    }

    #[test]
    #[should_panic(expected = "Command `;JMP` does not have a `comp` field.")]
    fn decode_err_missing_comp() {
        let inp = ";JMP";
        decode_command(&inp).unwrap();
    }
}