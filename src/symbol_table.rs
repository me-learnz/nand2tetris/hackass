//! Contains [`SymbolTable`] which keeps track of used symbols and their
//! memory addresses.
//!
//! [`SymbolTable`]: struct.SymbolTable.html
//!
//! # Examples
//!
//! ```rust
//! // Create a new symbol table
//! let mut symbol_table = SymbolTable::new();
//! assert!(symbol_table.contains("R0"));
//! assert_eq!(symbol_table.get_addr("R0"), Some(&0));
//!
//! // Add a new entry
//! symtable.add_entry(&String::from("foo"), 0);
//! assert_eq!(symbol_table.get_addr("foo"), Some(&0));
//!
//! // Add  new variables
//! assert_eq!(symtable.add_variable(String::from("bar")), 16));
//! assert_eq!(symtable.add_variable(String::from("baz")), 17);
//! assert_eq!(symtable.get_addr("bar"), Some(&16));
//! assert_eq!(symtable.get_addr("baz"), Some(&17));
//! ```

use std::collections::HashMap;

/// Keeps a table containing symbols and their corresponding memory addresses.
///
/// `SymbolTable` uses [`HashMap`] to store the table.
///
/// [`HashMap`]: https://doc.rust-lang.org/stable/std/collections/struct.HashMap.html
pub struct SymbolTable {
    table: HashMap<String, u16>,
    var_addr: u16,
}

impl SymbolTable {
    /// Create a new table with pre-defined symbols.
    pub fn new() -> SymbolTable {
        let default_table: HashMap<String, u16> = [
                (String::from("SP")    , 0),
                (String::from("LCL")   , 1),
                (String::from("ARG")   , 2),
                (String::from("THIS")  , 3),
                (String::from("THAT")  , 4),
                (String::from("SCREEN"), 16384),
                (String::from("KBD")   , 24576),
                (String::from("R0") , 0),
                (String::from("R1") , 1),
                (String::from("R2") , 2),
                (String::from("R3") , 3),
                (String::from("R4") , 4),
                (String::from("R5") , 5),
                (String::from("R6") , 6),
                (String::from("R7") , 7),
                (String::from("R8") , 8),
                (String::from("R9") , 9),
                (String::from("R10"), 10),
                (String::from("R11"), 11),
                (String::from("R12"), 12),
                (String::from("R13"), 13),
                (String::from("R14"), 14),
                (String::from("R15"), 15),
            ].iter().cloned().collect();
        SymbolTable {
            table: default_table,
            var_addr: 16,
        }
    }

    /// Add a new table entry in a form (symbol, address).
    pub fn add_entry(&mut self, symbol: &String, addr: u16) {
        self.table.insert(symbol.clone(), addr);
    }

    /// Checks whether symbol table contains a specified symbol.
    pub fn contains(&self, symbol: &str) -> bool {
        self.table.contains_key(symbol)
    }

    /// Get an address of a specified symbol. If the symbol does not
    /// exist in the table, return [`None`].
    ///
    /// [`None`]: https://doc.rust-lang.org/stable/std/option/enum.Option.html#variant.None
    pub fn get_addr(&self, symbol: &str) -> Option<&u16> {
        self.table.get(symbol)
    }

    /// Add a new variable entry with automatically assigned address.
    ///
    /// The address starts at 16 and is incremented by 1 with each new entry.
    pub fn add_variable(&mut self, name: &String) -> u16 {
        let addr = self.var_addr;
        self.add_entry(name, addr);
        self.var_addr += 1;
        addr
    }
}


#[cfg(test)]
mod tests {
    use super::{SymbolTable};

    #[test]
    fn contains() {
        let symtable = SymbolTable::new();
        assert!(symtable.contains("R0"));
        assert!(symtable.contains(&String::from("R0")));
        assert!(!symtable.contains("bagr"));
    }

    #[test]
    fn get_addr() {
        let symtable = SymbolTable::new();
        assert_eq!(symtable.get_addr(&String::from("R10")), Some(&10));
        assert_eq!(symtable.get_addr("KBD"), Some(&24576));
        assert_eq!(symtable.get_addr(&String::from("KBD")), Some(&24576));
        assert_eq!(symtable.get_addr(&String::from("xxx")), None);
    }

    #[test]
    fn add_entry() {
        let mut symtable = SymbolTable::new();
        let bagr = String::from("bagr");
        symtable.add_entry(&bagr, 0);
        assert_eq!(symtable.get_addr(&bagr), Some(&0));
        symtable.add_entry(&bagr, 10);
        assert_eq!(symtable.get_addr(&bagr), Some(&10));
    }

    #[test]
    fn add_variable() {
        let mut symtable = SymbolTable::new();
        let var1 = String::from("first");
        let var2 = String::from("second");
        let var3 = String::from("x_.X$:");

        assert_eq!(symtable.add_variable(&var1), 16);
        assert_eq!(symtable.add_variable(&var2), 17);
        assert_eq!(symtable.add_variable(&var3), 18);

        assert_eq!(symtable.get_addr(&var1), Some(&16));
        assert_eq!(symtable.get_addr(&var2), Some(&17));
        assert_eq!(symtable.get_addr(&var3), Some(&18));
    }
}